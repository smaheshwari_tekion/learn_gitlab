package com.bootcamp2023.chennaibootcamp.entity;

import jakarta.persistence.*;

import java.util.UUID;
//import org.springframework.data.mongodb.core.mapping.Document;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;
    private String sessionToken;
    @Column(name = "reset_token")
    private String resetToken;

    // constructors, getters, and setters
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String token) {
        this.sessionToken = token;
    }

//    public User(int id, String email, String password) {
//        this.id = id;
//        this.email = email;
//        this.password = password;
//    }

    public User() {
    }

    public void setResetToken(String resetToken) {
        this.resetToken= resetToken;
    }
}
