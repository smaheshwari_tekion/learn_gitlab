package com.bootcamp2023.chennaibootcamp.service;

import com.bootcamp2023.chennaibootcamp.entity.Note;
import com.bootcamp2023.chennaibootcamp.repo.NoteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NoteService {
    @Autowired
    private NoteRepo noteRepo;
    @Autowired
    private JavaMailSender javaMailSender;
    public Note addNote(Note note){
        return noteRepo.save(note);
    }
    public Note getNoteById(int id){
        return noteRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("Id not found"));
    }
    public List<Note> getAllNotes(){
        return noteRepo.findAll();
    }
    public Map<String, Object> sendNoteEmail(int id, String email){
        Map<String, Object> response = new HashMap<>();
        Note note = noteRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("not found"));

        String subject = note.getTitle();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject(subject);
        message.setText("Note: " + note.getId() + "\nContent: " + note.getContent());
        javaMailSender.send(message);
        response.put("success", true);
        response.put("error", null);
        return response;
    }
}
