package com.bootcamp2023.chennaibootcamp.service;

import com.bootcamp2023.chennaibootcamp.Utils.JwtUtils;
import com.bootcamp2023.chennaibootcamp.entity.User;
import com.bootcamp2023.chennaibootcamp.repo.UserRepo;
import jakarta.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private JavaMailSender javaMailSender;

    public Map<String, Object> register(String email, String password) {

        Map<String, Object> response = new HashMap<>();
        User user = new User();
        user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        user.setEmail(email);
        userRepo.save(user);
        response.put("success", true);
        response.put("error", null);
        return response;
    }

    public Map<String, Object> login(String email, String password) throws Exception {
        Map<String, Object> response = new HashMap<>();
        Optional<User> userOptional = userRepo.findByEmail(email);
        if (userOptional.isPresent()) {
            User user = userOptional.get();

            if (BCrypt.checkpw(password, user.getPassword())) {
                String token = jwtUtils.generateAccessToken(user);
                user.setSessionToken(token);
                userRepo.save(user);
                System.out.println("Hello");
                System.out.println(user.getSessionToken());
                response.put("success", true);
                response.put("error", null);
                response.put("token",token);
                return response;
            }

        }

        response.put("failure", true);
        return response;
    }

    public Map<String, Object> updatePass(String email, String newPassword){
        Map<String, Object> response = new HashMap<>();
        Optional<User> userOptional = userRepo.findByEmail(email);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            user.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
            userRepo.save(user);
            response.put("success", true);
            response.put("error", null);
            response.put("token",user.getSessionToken());
            return response;
        }
        response.put("failure", true);
        return response;
    }

    public String requestPasswordReset(String email){
        Optional<User> userOptional = userRepo.findByEmail(email);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            String resetToken = jwtUtils.generateAccessToken(user);
            user.setResetToken(resetToken);
            userRepo.save(user);
            SimpleMailMessage message = new SimpleMailMessage();
            String subject = "Password reset token";
            String body = "please use this token to reset your password:\n\n" + resetToken;
            message.setTo(email);
            message.setSubject(subject);
            message.setText(body);
            javaMailSender.send(message);
            return "Password reset token sent to " + email;
        }
        return "failure";

    }

    public String resetPassword(String email, String newPassword, String resetToken) {
        User user = userRepo.findByResetToken(resetToken);
        if (user==null) {
            return "Incorrect reset token for email "+ email;
        }
        userRepo.delete(user);
        user.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
        user.setResetToken(null);
        userRepo.save(user);
        return "Password reset successfully";
    }
}

