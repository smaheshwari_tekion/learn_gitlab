package com.bootcamp2023.chennaibootcamp.repo;
import com.bootcamp2023.chennaibootcamp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    //User findByEmail(String email);
    Optional<User> findByEmail(String email);
    User findByResetToken(String resetToken);
}
