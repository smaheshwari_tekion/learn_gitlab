package com.bootcamp2023.chennaibootcamp.repo;

import com.bootcamp2023.chennaibootcamp.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepo extends JpaRepository<Note, Integer> {
}
