package com.bootcamp2023.chennaibootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootApplication
public class ChennaiBootcampApplication {

	public static void main(String[] args) {
		try{
			SpringApplication.run(ChennaiBootcampApplication.class, args);

		} catch(Exception e){
			System.out.println(e);
		}
	}
}
