package com.bootcamp2023.chennaibootcamp.controller;

import com.bootcamp2023.chennaibootcamp.entity.User;
import com.bootcamp2023.chennaibootcamp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/SetPassLogin")
    public ResponseEntity<Map<String, Object>> register(@RequestParam String email, @RequestParam String password) throws Exception {
        return ResponseEntity.ok(userService.register(email, password));

    }
    @PostMapping("/PassLogin")
    public ResponseEntity<Map<String, Object>> login(@RequestParam String email, @RequestParam String password) throws Exception {
        return ResponseEntity.ok(userService.login(email, password));

    }
    @PutMapping("/UpdatePass")
    public ResponseEntity<Map<String, Object>> updateUserPassword(@RequestParam String email, @RequestParam String newPassword) throws Exception {
        return ResponseEntity.ok(userService.updatePass(email, newPassword));

    }
    @PostMapping("/ResetPass")
    public ResponseEntity<?> requestPasswordReset(@RequestParam String email) {
            String response = userService.requestPasswordReset(email);
            return ResponseEntity.ok(Map.of("success", true, "message", response));
    }

    @PostMapping("/ResetLink")
    public ResponseEntity<?> resetPassword(@RequestParam String email,@RequestParam String newPassword, @RequestParam String resetToken) {
        userService.resetPassword(email,newPassword, resetToken);
        return ResponseEntity.ok(Map.of("success", true, "error","null"));
    }
}
