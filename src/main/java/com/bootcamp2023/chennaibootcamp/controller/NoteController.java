package com.bootcamp2023.chennaibootcamp.controller;

import com.bootcamp2023.chennaibootcamp.entity.Note;
import com.bootcamp2023.chennaibootcamp.repo.NoteRepo;
import com.bootcamp2023.chennaibootcamp.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class NoteController {
    @Autowired
    private NoteRepo noteRepo;

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private NoteService noteService;

    @GetMapping("/notes")
    public ResponseEntity<Object> getAllNotes() {
        return ResponseEntity.ok(noteService.getAllNotes());
    }

    @GetMapping("/notes/{id}")
    public ResponseEntity<Object> getNoteById(@PathVariable int id) {
        return ResponseEntity.ok(noteService.getNoteById(id));
    }

    @PostMapping("/notes")
    public ResponseEntity<Object> addNote(@RequestBody Note note) {
        return ResponseEntity.ok(noteService.addNote(note));
    }

    @PostMapping("/notes/{id}/send")
    public ResponseEntity<Map<String, Object>> sendNoteEmail(@PathVariable int id, @RequestParam String email) {
        return ResponseEntity.ok(noteService.sendNoteEmail(id, email));
    }
}

